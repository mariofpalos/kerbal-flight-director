from cmd import Cmd     #For creating line-oriented command interpreters.
import os
import krpc

os.system('clear')      #This clears the terminal before running anything.

#this could go in the cmdpreloop() or whatever.
#try:
#    conn = krpc.connect(
#            name='Test',
#            address='192.168.1.130',
#            rpc_port=50000,
#            stream_port=50001)
#    print("Connected!")
#
#except:
#    print("Connection unsucessful")

class MyPrompt(Cmd):
    prompt = '> '
    intro = "Kerbal Flight Director v.0. Type ? to list commands"
    
    def do_connect(self, inp):
        '''Tries to (re)connect to the krpc server.'''
        try:
            conn = krpc.connect(
                    name='Main',
                    address='192.168.1.130',
                    rpc_port=50000,
                    stream_port=50001)
            print("Connected to kRPC server.")
            
            vessel = conn.space_center.active_vessel
            refframe = vessel.orbit.body.reference_frame
            
            print(vessel.name)
            
        except:
            print("Connection unsucessful.")
    
    def do_position(self, inp):
        '''Let's see how this handles streams'''
        with conn.stream(vessel.position, refframe) as position:
            while True:
                print(position())

    def do_countdown(self, inp):
        '''Initiates countdown'''
        print("Countdown initiated")
        countdown = True

    
    def do_FLIGHT(self, inp):
        '''Talk to FLIGHT'''
        print("Yes?")

    def do_exit(self, inp):
        '''Exit the application. Shorthand: x q Ctrl-D. Should add confirmation'''
        print("Shutting down.")
        return True     #So if I return ANYTHING the loop stops?

    def default(self, inp):
        
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)
        print("Default: {}".format(inp))
     
    do_EOF = do_exit
     
if __name__ == '__main__':
    MyPrompt().cmdloop()
