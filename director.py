import cmd2                     #For creating line-oriented command interpreters.
from cmd2 import COMMAND_NAME   #for categorizing documentation of commands.
import os
import krpc
import random
from tabulate import tabulate
#import RocketUtilities as ru

os.system('clear')              #This clears the terminal before running anything.

#async_alert() for receiving messages!

#VESSEL to talk to vessel, and alias it to specific spacecrafts
#How can I include the args in the tab completion? With subcommands?
#How to create permanent aliases?

class MyPrompt(cmd2.Cmd):

    prompt  =   '> '
    intro   =   "Kerbal Flight Director v.0. Type ? to list commands"
    
    def __init__(self):
        super().__init__()
        
        #Custom shortcuts
        shortcuts = cmd2.DEFAULT_SHORTCUTS
        shortcuts.update({'x': 'exit'})     #q is forbidden for some reason.
                                            #although it works as an alias of x!
        super().__init__(shortcuts=shortcuts)
        
        #Class variables
        self.conn           =   None
        self.activeVessel   =   None
        self.refframe       =   None
        self.countdown      =   False
   
    #def _preloop_hook(self) -> None:
        

    def do_countdown(self, args):
        print("Aight.")
        self.countdown = True
    
    ##  KRPC stuff ######################################## 

    def do_connect(self, args):
        """Tries to (re)connect to the krpc server."""

        try:
            self.conn = krpc.connect(
                    name='Main',
                    address='192.168.1.130',
                    rpc_port=50000,
                    stream_port=50001)

            self.poutput("Connected to kRPC server.")
            
            self.activeVessel = self.conn.space_center.active_vessel
            self.refframe = self.activeVessel.orbit.body.reference_frame
            
            self.poutput("Active vessel: {}".format(self.activeVessel.name)) 

        except:
            print("Connection unsucessful.")

    # Categorize the functions in the help list
    CMD_CAT_KRPC_STUFF = 'KRPC stuff'
    cmd2.categorize((do_connect), CMD_CAT_KRPC_STUFF)

    ##  Vessel control     ################################

    def setOnOff(self, subsystem, state):
        '''Generic function to turn stuff on or off. Hidden from the command list'''
        #UNTESTED!!!

        if subsystem and state == "on":
            #Asked to activate an already active subsystem.
            self.poutput("{} is already on".format(subsystem))

        elif subsystem and state == "off":
            #Asked to deactivate an active system.
            self.activeVessel.subsystem = False

        elif not subsystem and state == "on":
            #Asked to activate an inactive system.
            self.activeVessel.subsystem = True

        elif not subsystem and state == "off":
            #Asked to deactivate an already inactive system.
            self.poutput("{} is already off".format(subsystem))

        else:
            pass

    def do_VESSEL(self, args):
        '''
        Sends command to the active vessel.
        Use aliases if you want to call them by their names.
        '''
        if args == "ping":
            '''Pings the spacecraft'''
            if self.conn:
                self.poutput("pong")
            else:
                self.poutput("1 packet transmitted, 0 received, +1 errors, 100% packet loss")

        elif args.lower() == "rcs":
            '''Toggles RCS
            I need a generic function to toggle systems on and off (RCS, SAS, solar panels...'''

            ##Untested
            ##maybe try/except?
            self.setOnOff(self.activeVessel.control.rcs, off)

            #if self.activeVessel.control.rcs == True:
            #    self.activeVessel.control.rcs = False
            #else:
            #    self.activeVessel.control.rcs = True

    ##  Flight controllers ################################
    
    def do_FLIGHT(self, args):
        '''Talk to the Flight Dynamics Officer'''

        if  "status" in args:
            #self.poutput("The vehicle is not flying, so it's not my problem.")

            if self.conn:
                data = [["Situation", self.activeVessel.situation],
                        ["Reference frame", self.activeVessel.reference_frame],
                        ["Velocity (m/s)", self.activeVessel.flight(self.refframe).velocity],
                        ["Speed (m/s)", self.activeVessel.flight(self.refframe).speed]]
            else:
                data = [["Situation", "Unknown"],
                        ["Reference frame", "Unknown"],
                        ["Velocity (m/s)", "(-,-,-)"],
                        ["Speed (m/s)", "-"]]
            self.poutput(tabulate(data))

        elif "vel" in args.arg_list or "velocity" in args.arg_list: 
            self.poutput(self.activeVessel.flight(self.refframe).velocity)
        elif args == "speed":
            self.poutput(self.activeVessel.flight(self.refframe).speed)
        else:
            answers = ["What?", "Yes?", "Yes, director?"]
            self.poutput(random.choice(answers))

    def do_COMMS(self, args):
        '''Talk to the Communications officer'''

        if self.conn is not None:
            self.poutput("Connected to krpc server.")
        else:
            self.poutput("No connection.")


    # Categorize the functions in the help list
    CMD_CAT_FLIGHT_CONTROLLERS = 'Flight controllers'
    cmd2.categorize((do_FLIGHT, do_COMMS), CMD_CAT_FLIGHT_CONTROLLERS)

    ######################################################

    def do_exit(self, args):
        '''Exit the application. Shorthand: x q Ctrl-D. Should add confirmation'''
        print("Shutting down.")
        return True     #So if I return ANYTHING the loop stops?

    def default(self, args):
        """Not working properly with cmd2. Actually args are not working at all. At least in default."""        

        #print(args.arg_list[1]) not working

        self.poutput("Default: {}".format(args))
        
        if self.countdown == True and args == "GO":
            self.poutput("Launch!")
            self.countdown = False

    #A help category for cmd2's default commands. And it failed.
    #CMD_CAT_CMD2_DEFAULTS = 'cmd2 default commands'
    #cmd2.categorize((do_alias, do_edit, do_exit, do_history, do_py, do_pyscript, do_set, do_shortcuts, do_EOF, do_help, do_macro, do_run_script, do_shell), CMD_CAT_CMD2_DEFAULTS)
     
if __name__ == '__main__':
    import sys
    sys.exit(MyPrompt().cmdloop())

