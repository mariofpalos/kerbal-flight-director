import curses
import random
import time
import re
from nltk import word_tokenize

myscreen = curses.initscr()
myscreen.nodelay(1) # non interrupting getch
curses.curs_set(0)  # no cursor visible
curses.noecho()     # keys don't show on screen

vesselname = "Spurdo" # replace with vessel.name

# would it work if I chunk phrases using ","?
# make it detect "to the count of three!"

sentiment_dictionary = {
    "affirmatives": ("yes", "affirmative", "proceed", "do", "go", "continue",
    "good", "ok", "confirmed"),

    "negatives": ("no", "not", "negative", "bad", "wrong"),

    "positive_vocab": ("good", "ok", "nice", "nicely", "great", "awesome",
    "outstanding", "fantastic", "terrific", "like", "love", "wonderful", "thank",
    "thanks", "please", "sorry", "dear", "happy"),

    "negative_vocab": ("bad", "terrible", "crap", "useless", "hate",
    "fuck", "fucks", "shit", "turd", "dead", "death", "frog", "frogs"),

    "incrementers": ("too", "very", "so", "fuck", "fucking"),

    "decrementers": ("barely", "little"),

    "inverters": ("not", "no", "don't", "lack", "why", "can't"),
}

idioms = (

    (r'do you copy',
    ("%s to Ground Control, I've got you loud and clear." % vesselname,
    "Roger, Ground Control. Reading you loud and clear.",
    "Roger, CAPCOM.")),

    (r'open the pod bay doors',
    ("Well memed, my friend.")),

    (r'set phasers to stun',
    ("Well memed, my friend.")),

    (r'plz respond',
    ("No."))
)

numbers = [
    (("one", "first"), 1),
    (("two", "second"), 2),
    (("three", "third"), 3),
    (("four", "fourth"), 4),
    (("five", "fifth"), 5),
    (("six", "sixth"), 6),
    (("seven", "seventh"), 7),
    (("eight", "eighth"), 8),
    (("nine", "ninth"), 9),
    (("ten", "tenth"), 10),
]


class Crew(object):
    """
    To keep track of the stress of the crew and more stuff I don't even know yet.
    """

    def __init__(self):
        self.stress = 50


class Analyzer(object):
    """
    Accepts a string and returns the orders contained in it and the general sentiment
    """

    def __init__(self, entry):
        self.entry = entry #entry.lower()?
        self.tok = custom_tokenizer(entry)

    def is_fucking_confirmed(self):
        """
        What is hype may never die.
        """
        # add the idiom "make it so"
        fucking_confirmed = 0

        # if entry == previous question --> return True

        for n, word in enumerate(self.tok):

            if word in sentiment_dictionary["affirmatives"]:
                if n == 1 and self.tok[n-1] in sentiment_dictionary["inverters"]:
                    fucking_confirmed -=1
                elif n > 1 and ((self.tok[n-1] or self.tok[n-2]) in sentiment_dictionary["inverters"]):
                    fucking_confirmed -=1
                else:
                    fucking_confirmed += 1

            if word in sentiment_dictionary["negatives"]:
                if n == 1 and self.tok[n-1] in sentiment_dictionary["inverters"]:
                    fucking_confirmed +=1
                elif n > 1 and ((self.tok[n-1] or self.tok[n-2]) in sentiment_dictionary["inverters"]):
                    fucking_confirmed +=1
                else:
                    fucking_confirmed -= 1

        if fucking_confirmed > 0:
            return True
        elif fucking_confirmed < 0:
            return False
        else:
            return None

    def niceness(self):

        niceness = 0

        for n, word in enumerate(self.tok):
            #introduce inverters
            if word in sentiment_dictionary["positive_vocab"]:
                if self.tok[n-1] in sentiment_dictionary["incrementers"]:
                    niceness += 2
                elif self.tok[n-1] in sentiment_dictionary["inverters"]:
                    pass # so "no good" is neutral, but "bad" is negative, because it sounds worse.
                else:
                    niceness += 1
            if word in sentiment_dictionary["negative_vocab"]:
                if n == 1 and self.tok[n-1] in sentiment_dictionary["incrementers"]:
                    niceness -= 2
                elif n > 1 and ((self.tok[n-1] or self.tok[n-2]) in sentiment_dictionary["incrementers"]):
                    niceness -= 2
                else:
                    niceness -= 1

        return niceness

    def is_idiom(self):

        is_idiom = False

        # for n, item in enumerate(idioms):
        #    if item[0] in self.entry:
        #       is_idiom = True
        #       break

        for n, phrase in enumerate(idioms):
            if re.findall(phrase[0], self.entry):
                is_idiom = True
                break

        if is_idiom == True:
            return True, n

        else:
            return False, False

    def is_order(self):
        # what about "set blablabla to"?
        # and what with "don't close"?

        # Verb:
        verb = False
        for word in self.tok:
            if word in ["activate", "deploy", "open", "start"]:
                verb = "activate"
            elif word in ["deactivate", "retract", "close"]:
                verb = "deactivate"
            elif word in ["turn", "shut", "switch", "set"]:
                maybe = True
            elif word in ["on"] and maybe == True:
                verb = "activate"
            elif word in ["off", "down"] and maybe == True:
                verb = "deactivate"

        # Part:
        try:
            part = re.search(r'(engine|panel|radiator|light|bay|fairing|antenna|ladder)', self.entry).group()
        except:
            part = False

        # Number of:
        number = self.has_numbers()

        return verb, part, number

    def is_new_node(self):
        # return regex object?
        #return named tuples?
        # OK, ready to copy.
        pass

    def is_question(self):
        pass
        # ?$ so it's at the end

    def has_numbers(self):
        """
        Returns the numbers contained in the tokenized entry.
        doesn't recognize "retract two panels" as [1,2] for example
        """

        vector = []

        if "to" in self.tok:
            index = self.tok.index("to")
            if type(self.tok[index-1]) == int and type(self.tok[index+1]) == int:
                vector = range(self.tok[index-1], self.tok[index+1]+1)
            else:
                pass #canned response for "didn't understand"

        elif "all" in self.tok:
            vector = "max"

        else:
            for item in self.tok:
                if type(item) == int:
                    vector.append(item)

        if vector == []:
            return False
        else:
            return vector

# Out, Ready to copy, Roger so far, Say again/repeat , Wilco


#>>> def gender_features(word):
#...     return {'last_letter': word[-1]}
#>>> gender_features('Shrek')
#{'last_letter': 'k'}

def custom_tokenizer(entry):
    """
    Modified NLTK word tokenizer:
    - merges "do" and "ca" with "n't".
    - changes numbers from strings to integers
    Maybe this should look for idioms and fuse them together as well?
    """
    entry = entry.lower()
    tok = word_tokenize(entry)
    # the tokenizer goes nuts with "!!". Only happened once

    for n, word in enumerate(tok):
        if tok[n] == "n't" and tok[n-1] == "do":
                tok[n-1:n+1] = [''.join(tok[n-1:n+1])]
        elif tok[n] == "n't" and tok[n-1] == "ca":
                tok[n-1:n+1] = [''.join(tok[n-1:n+1])]
        else:
            try:
                tok[n] = int(word)
            except:
                for num in numbers:
                    if word in num[0]:
                        tok[n] = num[1]

    return tok

def open_channel():

    global log

    myscreen.addstr(maxy - 1, 0, "> "), myscreen.clrtoeol()

    myscreen.nodelay(0)
    curses.echo()
    entry = myscreen.getstr(maxy-1, 2, maxx-1)#.decode(encoding="utf-8") if python3
    myscreen.nodelay(1)
    curses.noecho()

    log.append("CAPCOM: " + entry)

    return entry


def replier(entry, response):

    if response == "roger":

        answers = [
            "Roger.",
            "Affirmative",
            "Got it.",
        ]

        response = random.choice(answers)

    if entry[-4:] == "over" or entry[-5:] == "over.":
        return response + " (Over)"
    else:
        return response

    regex = r'^(\w+)(?:, ?|: ?)(\w+)' # ^name, name OR ^name: name



log = []
prev_maxy, prev_maxx = myscreen.getmaxyx()

runmode = "running"

while runmode != "exit":

    # This adjusts the window if the size changes

    maxy, maxx = myscreen.getmaxyx()
    if (maxy != prev_maxy) or (maxx != prev_maxx):
        myscreen.clear()
    prev_maxy, prev_maxx = maxy, maxx

    myscreen.addstr(maxy - 1, 0, "Press ENTER to open voice channel, ESC to exit.")
    myscreen.clrtoeol()

    key = myscreen.getch()

    if key == curses.KEY_ENTER or key == 10 or key == 13: # 10: \n, 13: \r
        runmode = "listening"

    elif key == 27:

        myscreen.addstr(maxy - 1, 0, "Press ESC again to confirm.")
        myscreen.clrtoeol()

        myscreen.nodelay(0)
        key = myscreen.getch()
        myscreen.nodelay(1)
        if key == 27:
            runmode = "exit"
        else: # I believe this is unnecesary
            runmode = "running"


##############################################

    if runmode == "listening":
        # pending: wait 5s for additional lines, then merge them and analyze.

        entry = open_channel()

        if entry != "":
            if entry[0] == "/":

                if entry == "/exit":
                    break

                elif entry == "/warp":
                    log.append("- WARPING -")
                    runmode = "running"

            else:
                runmode = "analysis"
                #log.append("CAPCOM: " + entry)

##############################################

    if runmode == "analysis":

        meaning = Analyzer(entry)
        #meaning = Analyzer(open_channel())   it works!!!
        #response = ""

        idiom, n = meaning.is_idiom()
        verb, part, number = meaning.is_order()

        if idiom:
            response = ("Idiom number %s. ")%(n)


        elif verb != False or part != False or number != False:

            if verb and part and number:
                response = ("Action: %s, part: %s, quantity = %s. Should I proceed?")%\
                (verb, part, number)

                log.append(response)

                for item in reversed(log):
                    if line <= maxy:
                        myscreen.addstr(maxy - line, 0, item), myscreen.clrtoeol()
                        line += 1

                entry = open_channel()
                meaning = Analyzer(entry)
                #log.append("CAPCOM: " + entry)

                if meaning.is_fucking_confirmed():
                    response = ("Great!")



            elif verb and not part:
                response = ("%s what?")%(verb)
            elif verb and part and not number:
                response = ("But how many?")

        else:
            response = ("fucking confirmed: %s, niceness: %s. ")\
                %(meaning.is_fucking_confirmed(), meaning.niceness())

        #response = replier(meaning.entry, response)
        #log.append(replier(entry,response))
        log.append(response)

        runmode = "running"

##############################################

    if runmode == "order":
        pass

##############################################

    line = +3
    # Reprints the log on the screen

    for item in reversed(log):
        if line <= maxy:
            myscreen.addstr(maxy - line, 0, item), myscreen.clrtoeol()
            line += 1


curses.endwin()

# for item in log:
#     print item
