def twr(vessel):
    thrust = vessel.thrust
    weight = vessel.mass * vessel.orbit.body.surface_gravity
    return thrust / weight

def throttle_for_twr(desired_twr, vessel):
    thrust = vessel.mass * vessel.orbit.body.surface_gravity * desired_twr
    max_thrust = sum(engine.available_thrust for engine in active_engines(vessel))
    # what would happen if there are no active engines? an error?
    if max_thrust == 0:
        throttle = 0
    else:
        throttle = thrust / max_thrust

    if throttle <= 1:
        return throttle
    else:
        return 1


def throttle_for_G(desired_G, vessel):
    pass


def isp(vessel)
    pass

"""def stages(vessel):
    n = 0
    while True:
        try:
            vessel.resources_in_decouple_stage(stage=n, cumulative=False)
            n += 1
        except:
            break

    return n"""

def has_clamps(vessel):
    if len(vessel.parts.launch_clamps) != 0:
        return True
    else:
        return False



def active_engines(vessel):
    return filter(lambda e: e.active and e.has_fuel, vessel.parts.engines)

def fairing_number(vessel):
    return len(vessel.parts.fairings)

def batteries(vessel):
    return filter(lambda b: b.resources.has_resource("ElectricCharge")\
     and not b.engine, vessel.parts.all)
