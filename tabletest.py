import curses
import numpy as np

myscreen = curses.initscr()
myscreen.clear()
myscreen.nodelay(1)
curses.curs_set(0)
curses.noecho()

nicknames = [
    ("solarPanels2", "1x6 (shrouded)"),
    ("mk2DroneCore", "Drone core"),
    ("ksp.r.largeBatteryPack", "Large battery"),
]

def curses_table(parts, titles, variables):

    global line, nicknames


    column = 0

    for item in titles:

        myscreen.addstr(line, column, "{}".format(item))

        if column == 0:
            column += 20
        else:
            column += 12

    line += 1
    column = 0
    index = 1

    total = [0.0] * len(variables)

    for part in parts:

        for value in variables:

            if column == 0:

                name = value

                for nick in nicknames:
                    if value == nick[0]:
                        name = nick[1]

                myscreen.addstr(line, column, "{}: {}".format(index, name))
                column += 20
            else:
                myscreen.addstr(line, column, "{}".format(value))
                column += 12

        for n,i in enumerate(variables):
            if isinstance(i, str):
                variables[n] = 0.0

        total = [sum(x) for x in zip(total, variables)]

        line += 1
        column = 0
        index += 1

    return total

#######################################################################

runmode = "running"

while runmode != "exit":

    selection = myscreen.getch()

    if selection == ord("e") or selection == ord("E"):
        runmode = "exit"

    line = 0

    if runmode == "running":

        parts = (1, 2, 3, 4)
        titles = ("", "Status", "Flow (c/s)", "Exposure (%)")
        variables = ["ksp.r.largeBatteryPack",\
                    round(np.random.random(),2),\
                    round(np.random.random(),2),\
                    round(np.random.random(),2)]

        curses_table(parts, titles, variables)

    myscreen.refresh()

curses.endwin()
